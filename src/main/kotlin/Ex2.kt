/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/01
* TITLE: Rainbow
*/
import java.util.*

enum class COLORS{
    RED,ORANGE,YELLOW,GREEN,BLUE,INDIGO,VIOLET
}

fun main(){
    val sc = Scanner(System.`in`)
    val userColor = sc.next().uppercase()
    val rainbowColors = COLORS.values().any() {it.name == userColor}
    println(rainbowColors)
}