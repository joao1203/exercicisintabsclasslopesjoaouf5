import java.util.*
/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/03
* TITLE: Quiz
*/
val sc = Scanner(System.`in`)
var solvedQuestions: Int = 0

interface Question{
    fun printQuestion(num: Int)
    fun solveQuestion()
}

class FreeTextQuestion(private val prompt: String, val answer: String): Question{
    private var isResolved: Boolean = false

    override fun printQuestion(num: Int) {
        println("Question ${num+1}:")
        println(this.prompt)
    }

    override fun solveQuestion() {
        do {
            println("Write the correct answer:")
            val userAnswer = sc.nextLine().uppercase()

            if (this.answer == userAnswer) {
                this.isResolved = true
                solvedQuestions++
                println("Congratulations, $userAnswer is the correct answer!")
            }
            else println("That is not the correct answer...")
        } while (!this.isResolved && userAnswer != "CANCEL")
    }
}

class MultipleChoiceQuestion (private val prompt: String, val option1 : String, val option2: String, val option3: String, val option4: String, val answer: Char): Question{
    private var isResolved: Boolean = false
    override fun printQuestion(num: Int) {
        println("Question ${num+1}:")
        println(this.prompt)
        println("·${this.option1}\n·${this.option2}\n·${this.option3}\n·${this.option4}")
    }

    override fun solveQuestion() {
        do {
            println("Write the correct answer:")
            val userAnswer = sc.next().uppercase().single()

            if (this.answer == userAnswer) {
                this.isResolved = true
                solvedQuestions++
                println("Congratulations, $userAnswer is the correct answer!")
            }
            else println("That is not the correct answer...")
        } while (!this.isResolved)
    }
}

fun main(){
    val questionsGet = Ex4Questions.questionList
    for (i in questionsGet.indices){
        val question = questionsGet[i]
        question.printQuestion(i)
        question.solveQuestion()
    }

    println("You've solved $solvedQuestions out of ${questionsGet.size} questions!")
}
