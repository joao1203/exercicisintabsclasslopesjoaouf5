/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/02
* TITLE: AutonomousCarPrototype
*/

interface CarSensors{
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
}

enum class Direction{
    FRONT,LEFT,RIGHT
}

class AutonomousCar(): CarSensors{
    override fun isThereSomethingAt(direction: Direction): Boolean {
        TODO("Check if the following position is occupied or not.")
    }

    override fun go(direction: Direction) {
        TODO("Go to following direction.")
    }

    override fun stop() {
        TODO("Stop the car.")
    }

    fun doNextNSteps(times: Int){
        for (i in 1..times){
            if (!isThereSomethingAt(Direction.FRONT)) go(Direction.FRONT)
            else {
                if (!isThereSomethingAt(Direction.RIGHT)) go(Direction.RIGHT)
                else {
                    if (!isThereSomethingAt(Direction.LEFT)) go(Direction.LEFT)
                    else stop()
                }
            }
        }
    }
}

fun main() {
    val autoCar = AutonomousCar()

    autoCar.doNextNSteps(5)
}