/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/01
* TITLE: StudentWithTextGrade
*/

enum class GRADES{
    FAILED,PASSED,GOOD,NOTICEABLE,EXCELLENT
}

data class Student(val name: String, val textGrade: GRADES)

fun main(){
    val student1 = Student("Mar",GRADES.FAILED)
    val student2 = Student("Joan",GRADES.EXCELLENT)
    println(student1)
    println(student2)
}