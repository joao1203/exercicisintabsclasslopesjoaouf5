class Ex4Questions {
    companion object{
        val questionList = listOf<Question>(
            FreeTextQuestion("What is the name of the star found on the center of our planetary system?","SUN"),
            MultipleChoiceQuestion("How many fingers do humans tipically have?","A.5","B.10","C.15","D.20",'B'),
            MultipleChoiceQuestion("Which is not a primary color?","A.Red","B.Blue","C.Yellow","D.Green",'D'),
            FreeTextQuestion("Which animal is known as 'The King of the Jungle'?","LION")
        )
    }
}