/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/06
* TITLE: GymControlApp
*/
import java.util.*

interface GymControlReader{
    fun nextId(): String
}

class GymControlManualReader(val scanner:Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}

fun main() {
    val gymCMR = GymControlManualReader()
    val gymMemberIdList = mutableListOf<String>()

    for (id in 1..8){
        val gymId = gymCMR.nextId()

        if (gymId !in gymMemberIdList) {
            gymMemberIdList.add(gymId)
            println("$gymId ENTRADA")
        }
        else{
            gymMemberIdList.remove(gymId)
            println("$gymId SORTIDA")
        }
    }
}