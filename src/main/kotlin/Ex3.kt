/*
* AUTHOR: Joao Lopes Dias
* DATE: 2023/03/01
* TITLE: InstrumentSimulator
*/

abstract class Instrument {
    open fun makeSounds(times: Int){
        println("clink")
    }
}

class Drum(var tone: String): Instrument() {
    init {
        when (tone){
            "A" -> tone = "TAAAM"
            "O" -> tone = "TOOM"
            "U" -> tone = "TUUM"
        }
    }
    override fun makeSounds(times: Int) {
        repeat(times){
            println(tone)
        }
    }
}

class Triangle(private val resonance: Int): Instrument() {
    var sound = "T"
    init {
        if (resonance in 1..5){
            repeat(resonance){
                sound += "I"
            }
            sound += "NC"
        }
        else {
            println("$resonance is not inside the acceptable range. Sound set to 'TIIIIINC'.")
            sound = "TIIIIINC"
        }
    }
    override fun makeSounds(times: Int) {
        repeat(times){
            println(sound)
        }
    }
}

fun main(){
    val instruments: List<Instrument> = listOf(Drum("A"),Drum("U"),Drum("O"),Triangle(2),Triangle(4),Drum("A"),Triangle(6))

    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (item in instruments) {
        item.makeSounds(2)
    }
}
